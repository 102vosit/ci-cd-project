terraform {
  backend "s3" {
    bucket = "vosit-samarkand-2021"
    key    = "tstate/gogreen.tfstate"
    region = "us-east-1"
  }
}
