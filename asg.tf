##############################
## Web AutoScaling Group    ##
##############################
# Launcher Template for Web
resource "aws_launch_configuration" "web_launcher" {
  name            = "web_launcher"
  image_id        = "ami-047a51fa27710816e"
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.web-elb-sg.id]
  user_data       = <<-EOF
                #!/bin/bash -ex
                yum -y install httpd php mysql php-mysql
                chkconfig httpd on
                service httpd start
                if [ ! -f /var/www/html/lab-app.tgz ]; then
                cd /var/www/html
                wget https://aws-tc-largeobjects.s3-us-west-2.amazonaws.com/CUR-TF-200-ACACAD/studentdownload/lab-app.tgz
                tar xvfz lab-app.tgz
                chown apache:root /var/www/html/rds.conf.php
                fi
                EOF
  lifecycle {
    create_before_destroy = true
  }
}

# Auto Scaling Group for Web tier
resource "aws_autoscaling_group" "web-asg" {
  launch_configuration = aws_launch_configuration.web_launcher.name
  vpc_zone_identifier  = [aws_subnet.private-subnet-1.id, aws_subnet.private-subnet-2.id]
  min_size             = 3  ## at least
  max_size             = 12 ## no more
  desired_capacity     = 5  ## usual average
  target_group_arns    = [aws_lb_target_group.Web-GoGreen-TargetGroup.arn]
  health_check_type    = "ELB"
  tag {
    key                 = "Name"
    value               = "web-asg"
    propagate_at_launch = true
  }
}
